package Bigler.Josie.Hangman;

import java.io.*;
import java.util.*;


public class Main {

    public static void main(String[] args) throws IOException {
        //this is me reading my text file and defining and initializing my object file
        BufferedReader file = new BufferedReader(new FileReader("C:\\Users\\Owner\\IdeaProjects\\HangMan\\out\\production\\HangMan\\words.txt"));


        int guessCounter = 10;
        System.out.print("Welcome to my Hangman Game.  I made this program in Java in less lines than \n than Stephen Scott, who's nothing but" +
                " a two bit \n hacker who wishes HE COULD GET ON MY LEVEL!\n" +
                "anyway, you have " + guessCounter + " guesses to figure out the word. \nOnly the first character you input will be" +
                "considered.  \nGood luck");

        //Me creating my list called myWordList
        ArrayList<String> myWordList = new ArrayList<String>();

        Scanner scanner = new Scanner(System.in);


        //My while loop, adding words to my list
        String line;
        while ((line = file.readLine()) != null) {
            myWordList.add(line.toLowerCase());
        }


            /*Me creating a random number, which then picks a random word to use as the solution from your text file.
            * the variable screen is used as the user interface to see what correct guesses they have made.
            * The variable solution is the variable that stores the randomly selected word as an array of char*/
            Random randomNumber = new Random();
            int storedNumber = randomNumber.nextInt(myWordList.size());
            char[] solution = new char[(myWordList.get(storedNumber)).length()];
            myWordList.get(storedNumber).getChars(0, (myWordList.get(storedNumber)).length(), solution, 0);
            char[] screen = new char[solution.length];
            {
                Arrays.fill(screen, '_');
            }

            //This is a hashset used to store the incorrect guesses made by the player.
            HashSet<Character> wrongAnswers = new HashSet<Character>();

            //this variable is created to use my Checker method. gc is short for guessChecker.
            Main gc = new Main();

            //This while loop is the main body of the game, constantly ch
            while (guessCounter >= 0) {
                if (guessCounter <= 0) {
                    System.out.print("\n Sorry, you're a regular old Steven Scott, you lose.");
                    guessCounter = -1;

                } else {
                    char guess = scanner.next().charAt(0);
                    gc.checker(guess, solution, screen, wrongAnswers);

                    for (char i : screen) {
                        System.out.print(i + " ");
                    }
                    System.out.print("\nWrong Character Guesses:" + wrongAnswers + "\nNumber of Incorrect Guesses left: " + (guessCounter));
                    boolean wrongGuess = gc.checker(guess, solution, screen, wrongAnswers);
                    if (wrongGuess == false) {
                        guessCounter--;
                    }
                    if (Arrays.equals(screen, solution)) {
                        System.out.print("\n You Win!");
                        guessCounter = -1;
                    }
                }
            }

    }
    public boolean checker(char guessAnswer, char[] solutionArray, char[] screenArray, HashSet wrongAnswersSet) {
        int i;
        boolean correctGuess = false;
        for (i = 0; i < solutionArray.length; i++) {
            if (guessAnswer == solutionArray[i]) {
                screenArray[i] = guessAnswer;
                correctGuess = true;

            }
        }
        if (correctGuess == false) {
            wrongAnswersSet.add(guessAnswer);

        }
        return correctGuess;
    }
}




